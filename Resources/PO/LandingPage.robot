*** Settings ***
Documentation  Amazon page object example
Library  SeleniumLibrary
*** Variables ***
${NAV_IPER_E_SUPER} =  xpath=//a[@href="/iper-e-super"]
${NAV_MOTORI} =  xpath=//a[@href="/motori"]
${NAV_ARREDAMENTO} =  xpath=//a[@href="/arredamento"]
${NAV_BRICOLAGE} =  xpath=//a[@href="/bricolage"]
${NAV_DISCOUNT} =  xpath=//a[@href="/discount"]
${NAV_ELETTRONICA} =  XPATH=//a[@href="/elettronica"]
${SEARCH_TERM} =  Lamborghini
${SEARCh_TEXT_BOX_DVC} =  css=#bb-geoform-addressform > div.geolocator__text > div.geolocator__input > input
${SEARCH_TERM_DOVECONVIENE} =  09010, Italia

*** Keywords ***
Search for Products
    Enter Search Term
    Subumit Search

Load and tell me what you see things
    Wait Until Page Contains  Hi people

Enter Search Term
    Input Text  id=twotabsearchtextbox  ${SEARCH_TERM}

Enter Position Doveconviene
    Input Text  ${SEARCh_TEXT_BOX_DVC}  ${SEARCH_TERM_DOVECONVIENE}

Click On Vetrina Nav Bar
    Click Button  css=#bb-mainHeaderDesktopMenu > div > div.tabs__wrapper > div.tabs__scrolling > div.tabs__tab.color--primary > a > div > div.button__text

Submit Localization
    Click Link  id=bb-geoform-addressform

Click On Iper Super Nav Bar
    Click Link  ${NAV_IPER_E_SUPER}

Click On Elettronica Nav Bar
    Click Link  ${NAV_ELETTRONICA}

Click On Discount Nav Bar
    Click Link  ${NAV_DISCOUNT}

Click On Motori Nav Bar
    Click Link  ${NAV_MOTORI}

Click On Arredamento Nav Bar
    Click Link  ${NAV_ARREDAMENTO}

Click On Bricolage Nav Bar
    Click Link  ${NAV_BRICOLAGE}

Subumit Search
    Click Burron  css=#body > div:nth-child(21)

Verify Product Search Completed
    Wait Until Page Contains  risultati in "${SEARCH_TERM}"

Verify iper e super get loaded
    Wait Until Page Contains  Volantini Iper e super nelle vicinanze

Verify home page get loaded
    Wait Until Page Contains  Volantini in vetrina nelle vicinanze

Verify Location get Loaded
    Wait Until Page Contains  09045 Quartu Sant'Elena CA, Italia

Scroll down dvc
    Execute JavaScript    window.scrollTo(0, document.body.scrollHeight)

Get all flyers
    ${elements}=    Get WebElements    //div[@class='flyersGrid js-flyersGrid']
    Click Button    ${elements}[0]

Flip over up volantino
    Click Button  css=#js-viewer > div > div > div.css-ogt2jz.enxjfe0 > div.css-prlm3g.e1xkad831 > span > svg
#    FOR    ${element}    IN    @{elements}
#    Click Button   ${element}
#    Log    ${element.text}
#    END

Click Product Link
    [Documentation]  Clicks on the first prodcuct in the search results list
    Click Link  Click Link  xpath=/html/body/div[1]/div[2]/div[1]/div[2]/div/span[3]/div[1]/div[1]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h5/a

