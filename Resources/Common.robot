*** Settings ***
Library  SeleniumLibrary
*** Variables ***
*** Keywords ***
Begin Web Test
    #Open Browser  ${START_URL}  ${BROWSER} 
    #Open Browser  ${START_URL}  firefox
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys
    Call Method    ${chrome_options}    add_argument    test-type
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Call Method    ${chrome_options}    add_argument    --disable-dev-shm-usage
    #Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}  executable_path=/Users/fabiolima/.rvm/gems/ruby-2.4.2/bin/chromedriver
    Create Webdriver    Chrome    my_alias    chrome_options=${chrome_options}  executable_path=/usr/bin/chromedriver
    # Maximize Browser Window  # doesn't work under XVFB
    Set Window Size    1200    1000
    Go To    ${START_URL}
End Web Test
   Close Browser
Insert Testing Data
    Log  I am setting up the test data...
Cleanup Testing Data
    Log  I am cleaning up the test data...