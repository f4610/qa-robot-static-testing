*** Settings ***
Resource  ${EXECDIR}/Resources/PO/LandingPage.robot
*** Variables ***
${BROWSER} =  chrome
${START_URL} =  http://deeplinka.surge.sh/
${SEARCH_TERM} =  Lamborghini

*** Keywords ***
Landing depeelinka page
    LandingPage.Load and tell me what you see things

User Lands in the doveconviene main page
    LandingPage.Verify home page get loaded
    LandingPage.Scroll down dvc
    LandingPage.Get all flyers

User Will Click On Vetrina
    LandingPage.Verify home page get loaded
    LandingPage.Click On Vetrina Nav Bar

User Will Click On Iper Super
    LandingPage.Verify home page get loaded
    LandingPage.Click On Bricolage Nav Bar
    LandingPage.Click On Arredamento Nav Bar
    LandingPage.Click On Discount Nav Bar
    LandingPage.Click On Iper Super Nav Bar
    Verify iper e super get loaded

Search for Location
    LandingPage.Verify home page get loaded
    LandingPage.Enter Position Doveconviene
    LandingPage.Press Native Enter
    LandingPage.Click On Iper Super Nav Bar
    LandingPage.Verify iper e super get loaded
